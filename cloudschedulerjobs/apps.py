from django.apps import AppConfig


class Config(AppConfig):
    """Configuration for Cloud Scheduler jobs application."""
    #: The short name for this application. This should be the Python module
    #: name since Django uses this to import things.
    name = 'cloudschedulerjobs'

    #: The human-readable verbose name for this application.
    verbose_name = 'Cloud Scheduler jobs application'

    def ready(self):
        """
        Perform application initialisation once the Django platform has been initialised.

        """
        super().ready()

        # Import, and thereby register, our custom system checks
        from . import systemchecks  # noqa: F401
