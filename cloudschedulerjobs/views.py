import logging

import google.auth.transport.requests
import requests
from cachecontrol import CacheControl
from django.conf import settings
from django.core.exceptions import ValidationError
from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from google.oauth2 import id_token

LOG = logging.getLogger('cloudschedulerjobs')


# Create, cache and use a persistent session in subsequent calls of verify_oauth2_token()
cached_session = CacheControl(requests.session())


def verify_oauth2_token(token, audience, session=cached_session):
    """Verifies an OIDC token."""
    request = google.auth.transport.requests.Request(session=session)

    return id_token.verify_oauth2_token(
        token, request, audience=audience)



class CloudSchedulerView(View):
    """
    View to serve Cloud Scheduler requests.

    The Cloud Scheduler job can be provided as `job_callable` method of the Class view.

    """
    def job_callable(): return None

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(CloudSchedulerView, self).dispatch(request, *args, **kwargs)

    def post(self, request):
        # Verify that the post request originates from Cloud Scheduler.
        try:
            # Get the Cloud Scheduler-generated JWT in the "Authorization" header.
            bearer_token = request.headers.get('Authorization')
            token = bearer_token.split(' ')[1]

            claim = verify_oauth2_token(token, settings.CLOUD_SCHEDULER_AUDIENCE)

            if claim["email"] not in settings.CLOUD_SCHEDULER_SERVICE_ACCOUNT_IDENTITIES \
                    or claim["email_verified"] is not True:
                raise ValidationError

        except Exception:
            return JsonResponse({'message': 'JWT validation failed'}, status=400)

        try:
            self.job_callable()
        except Exception:
            LOG.exception('Failed to call function %s', self.job_callable.__name__)
            return JsonResponse({'message': 'Task failed'}, status=500)

        return JsonResponse({'status': 'ok'})
