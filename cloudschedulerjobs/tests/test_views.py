from unittest.mock import patch

import cloudschedulerjobs
from django.conf import settings
from django.test import TestCase
from django.urls.base import reverse
from rest_framework.test import APIRequestFactory


class CloudSchedulerViewTests(TestCase):
    """
    CloudSchedulerView unittests.

    """

    def setUp(self):
        super().setUp()

        # Instantiate RequestFactory
        self.factory = APIRequestFactory()

    @patch('cloudschedulerjobs.views.verify_oauth2_token')
    def test_valid_oauth2_token(self, mock_verify_oauth2_token):
        """A valid oauth2 token should validate."""

        # Mock the claim returned by verify_oauth2_token
        claim = {}
        claim["email"] = settings.CLOUD_SCHEDULER_SERVICE_ACCOUNT_IDENTITIES[0]
        claim["email_verified"] = True

        mock_verify_oauth2_token.return_value = claim

        # Checks the http response after a successful token verification

        # NOTE: The following does not check the functionality of the scheduled
        # jobs, which should be addressed elsewhere. Instead, it creates
        # a CloudSchedulerView view instance with a test callable job and
        # checks the HTTP response when querying any scheduled job endpoints.

        # Mock a scheduled job
        def test_job(): return None

        # Override CloudSchedulerView's job_callable
        view = cloudschedulerjobs.views.CloudSchedulerView.as_view(job_callable=test_job)

        request = self.factory.post(reverse('test-job'), HTTP_AUTHORIZATION='Token ABC')

        # Make an HTTP POST request
        response = view(request)

        # Check
        self.assertEqual(200, response.status_code)

    @patch('cloudschedulerjobs.views.verify_oauth2_token')
    def test_invalid_oauth2_token(self, mock_verify_oauth2_token):
        """An invalid oauth2 token should not validate."""

        # Mock the claim returned by verify_oauth2_token
        claim = {}
        claim["email"] = settings.CLOUD_SCHEDULER_SERVICE_ACCOUNT_IDENTITIES[0]
        # Unverified email to fail the token validation
        claim["email_verified"] = False

        # Mock verify_oauth2_token
        mock_verify_oauth2_token.return_value = claim

        # Checks the http response after a successful token verification

        # Mock a scheduled task
        def test_job(): return None

        # Override CloudSchedulerView's job_callable
        view = cloudschedulerjobs.views.CloudSchedulerView.as_view(job_callable=test_job)

        # The URL can be any one of the scheduled jobs
        request = self.factory.post(reverse('test-job'), HTTP_AUTHORIZATION='Token ABC')

        # Make an HTTP POST request
        response = view(request)

        # Check
        self.assertEqual(400, response.status_code)

    @patch('cloudschedulerjobs.views.verify_oauth2_token')
    def test_failed_function(self, mock_verify_oauth2_token):
        """A failed function should be handled."""

        # Mock the claim returned by verify_oauth2_token
        claim = {}
        claim["email"] = settings.CLOUD_SCHEDULER_SERVICE_ACCOUNT_IDENTITIES[0]
        claim["email_verified"] = True

        # Mock verify_oauth2_token
        mock_verify_oauth2_token.return_value = claim

        # Checks the http response code after a failed scheduled job

        # Mock a scheduled task that fails
        def test_job(): raise Exception

        # Override CloudSchedulerView's job_callable
        view = cloudschedulerjobs.views.CloudSchedulerView.as_view(job_callable=test_job)

        # The URL can be any one of the scheduled jobs
        request = self.factory.post(reverse('test-job'), HTTP_AUTHORIZATION='Token ABC')

        # Make an HTTP POST request
        response = view(request)

        # Check
        self.assertEqual(500, response.status_code)
