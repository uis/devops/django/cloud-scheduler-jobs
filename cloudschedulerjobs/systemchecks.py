"""
The :py:mod:`cloudschedulerjobs` application ships with some custom system checks which ensure that required
settings are present. These system checks are registered by the :py:class:`~cloudschedulerjobs.apps.Config`
class's :py:meth:`~cloudschedulerjobs.apps.AssetsConfig.ready` method.

.. seealso::

    The `Django System Check Framework <https://docs.djangoproject.com/en/2.0/ref/checks/>`_.

"""
from django.conf import settings
from django.core.checks import Error, register


@register
def cloud_scheduler_required_settings_check(app_configs, **kwargs):
    """
    A system check ensuring that the Cloud Scheduler required settings have non-empty values.

    .. seealso:: https://docs.djangoproject.com/en/2.0/ref/checks/

    """
    errors = []

    CLOUD_SCHEDULER_REQUIRED_SETTINGS = [
        'CLOUD_SCHEDULER_AUDIENCE',
        'CLOUD_SCHEDULER_SERVICE_ACCOUNT_IDENTITIES',
    ]

    for idx, name in enumerate(CLOUD_SCHEDULER_REQUIRED_SETTINGS):
        value = getattr(settings, name, None)
        if value is None or value == '':
            errors.append(Error(
                'Required setting {} not set'.format(name),
                id='cloudschedulerjobs.E{:03d}'.format(idx + 1),
                hint='Add {} to settings.'.format(name)))

    return errors
