from django.conf.urls import url

from cloudschedulerjobs.views import CloudSchedulerView


# An example of a scheduled job executed in CloudSchedulerView.
def test_job(): return {'result': 'ok'}


urlpatterns = [
    url(r'^test-job/$', CloudSchedulerView.as_view(job_callable=test_job), name='test-job'),
]
