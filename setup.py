#!/usr/bin/env python

from distutils.core import setup

from setuptools import find_packages

setup(
    name='django-cloudschedulerjobs',
    version='0.1',
    packages=find_packages(),
    include_package_data=True,
    license='MIT',
    description=(
        'Utilities for creating Cloud Scheduler HTTP endpoints.'
    ),
    long_description=open('README.md').read(),
    url='https://gitlab.developers.cam.ac.uk/uis/devops/django/cloud-scheduler-jobs.git',
    author='DevOps team, University Information Services, University of Cambridge',
    author_email='devops@uis.cam.ac.uk',
    install_requires=[
        'django',
        'djangorestframework',
        'requests',
        'cachecontrol',
        'google-auth',
        'mock',
    ],
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
    ],
)
