# Introduction

This project is a Django app that provides a generic HTTP endpoint for Cloud Scheduler
jobs created on Google Cloud Platform.

The Django view `CloudSchedulerView` takes as argument a callable function that will
run once the view is invoked.

In your Django application's `urls.py` file, you can add an entry for a Cloud Scheduler
job endpoint as follows:
```python
from django.conf.urls import url

from cloudschedulerjobs.views import CloudSchedulerView
from jobs import my_job


urlpatterns = [
    url(r'^run-my-job/$', CloudSchedulerView.as_view(job_callable=my_job), name='run-my-job'),
]
```

Note that `test-job/` is an existing endpoint included in the app for testing.

# Use

Install `cloudschedulerjobs` using pip:

```bash
pip install git+https://gitlab.developers.cam.ac.uk/uis/devops/django/cloud-scheduler-jobs.git
```

Add `cloudschedulerjobs` to the installed applications in the project configuration:

```python
    INSTALLED_APPS=(
    ...
        'cloudschedulerjobs',
    ...
    ),
```

and in your project's `urls.py` file:
```python
    urlpatterns = [
       ...
       path('cloudschedulerjobs/', include('cloudschedulerjobs.urls')),
       ...
    ]
```

# Test

To create a virtual environment (a one-off command):

```
virtualenv -p python3.7 venv
```

To activate the virtual environment (run for every new session):

```
source venv/bin/activate
```

To install the requirements from the setup.py (a one-off command):

```
pip install -e .
```

To run the test suite:
```
python runtests.py
```

Alternatively, given [docker-compose](https://docs.docker.com/compose/install/) is installed you can run:
```
docker-compose up
```

# Configuration

The app's utilities require the following Django settings to be defined within
the including project.

| Setting | Description |
| ------- | ----------- |
| CLOUD_SCHEDULER_AUDIENCE | The audience claim used in OAUTH2 token verification. |
| CLOUD_SCHEDULER_SERVICE_ACCOUNT_IDENTITIES | List of Service Account identities that are allowed in the OAUTH2 token email claim. |
