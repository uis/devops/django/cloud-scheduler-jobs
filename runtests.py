import logging
import os
import sys
import django
from django.conf import settings
from django.test.runner import DiscoverRunner


DIRNAME = os.path.dirname(__file__)

settings.configure(
    DEBUG=True,
    SECRET_KEY='placeholder',
    CLOUD_SCHEDULER_SERVICE_ACCOUNT_IDENTITIES='placeholder',
    CLOUD_SCHEDULER_AUDIENCE='placeholder',
    DATABASES={'default': {'ENGINE': 'django.db.backends.sqlite3', 'NAME': 'test.db', }},
    TIME_ZONE='Europe/London',
    USE_TZ=True,
    ROOT_URLCONF='cloudschedulerjobs.urls',
    INSTALLED_APPS=(
        'django.contrib.contenttypes',
    ),
)

django.setup()

logging.basicConfig()
test_runner = DiscoverRunner()

failures = test_runner.run_tests(['cloudschedulerjobs', ])
if failures:
    sys.exit(failures)
